#include "Mesh.h"

#include <cstddef>

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices)
    : myVertexCount(vertices.size()),
      myIndexCount(indices.size())
{
    glCreateVertexArrays(1, &myVao);
    glBindVertexArray(myVao);

    glCreateBuffers(1, &myVbo);
    glCreateBuffers(1, &myEbo);

    // Bind and buffer our vertex data
    glBindBuffer(GL_ARRAY_BUFFER, myVbo);
    glBufferData(GL_ARRAY_BUFFER, 
                 vertices.size() * sizeof(Vertex), 
                 vertices.data(), 
                 GL_STATIC_DRAW);
    
    // Bind and buffer our index data
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myEbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 indices.size() * sizeof(GLuint),
                 indices.data(),
                 GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Position));
    
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Color));

    glBindVertexArray(GL_NONE);
}

Mesh::~Mesh()
{
    glDeleteBuffers(1, &myVbo);
    glDeleteBuffers(1, &myEbo);

    glDeleteVertexArrays(1, &myVao);
}

void Mesh::Draw()
{
    glBindVertexArray(myVao);

    glDrawElements(GL_TRIANGLES, myIndexCount, GL_UNSIGNED_INT, nullptr);
}
