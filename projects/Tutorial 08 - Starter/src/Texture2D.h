#pragma once

#include <glad/glad.h>
#include <memory>
#include <string>
#include <EnumToString.h>

ENUM(InternalFormat, GLint,
     Depth = GL_DEPTH_COMPONENT,
     DepthStencil = GL_DEPTH_STENCIL,
     R8 = GL_R8,
     R16 = GL_R16,
     RGB8 = GL_RGB8,
     RGB16 = GL_RGB16,
     RGBA8 = GL_RGBA8,
     RGBA16 = GL_RGBA16,
);

ENUM(PixelFormat, GLint,
     Red = GL_RED,
     Rg = GL_RG,
     Rgb = GL_RGB,
     Bgr = GL_BGR,
     Rgba = GL_RGBA,
     Bgra = GL_BGRA,
     Depth = GL_DEPTH_COMPONENT,
     DepthStencil = GL_DEPTH_STENCIL
);ENUM(PixelType, GLint,
     UByte = GL_UNSIGNED_BYTE,
     Byte = GL_BYTE,
     UShort = GL_UNSIGNED_SHORT,
     Short = GL_SHORT,
     UInt = GL_UNSIGNED_INT,
     Int = GL_INT,
     Float = GL_FLOAT
);// Represents a 2D texture in OpenGL
class Texture2D
{
    struct Description {
        uint32_t Width = 0;
        uint32_t Height = 0;
        InternalFormat Format = InternalFormat::RGBA8;
    };
    
public:
    using Sptr = std::shared_ptr<Texture2D>;

    explicit Texture2D(const Description& description);
    virtual ~Texture2D();
    void LoadData(void* data, size_t width, size_t height, PixelFormat format, PixelType type);

    void Bind(int slot) const;
    static void UnBind(int slot);

    static Sptr LoadFromFile(const std::string& fileName, bool loadAlpha = true);
protected:
    GLuint myTextureHandle;
    Description myDescription;
    
    void __SetupTexture();
};