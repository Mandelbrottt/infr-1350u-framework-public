/*
 * Myles Cragg
 * 100704245
 * Lecture 3 main.cpp
 * Draw 2 triangles on the screen as a bonus
 *
 * Extra little bit i did: use 2 attributes so
 * each tri has a different color
 */

#include <iostream>

#include "Logging.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <GLM/glm.hpp>

GLFWwindow* g_window;

int main()
{
    Logger::Init();

    if (!glfwInit())
    {
        LOG_ERROR("Failed to initialize GLFW!");
        return 1;
    }

    g_window = glfwCreateWindow(800, 600, "CG Intro", NULL, NULL);

    glfwMakeContextCurrent(g_window);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        LOG_ERROR("Failed to initialize GLAD!");
        return 2;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback([](GLenum        source,
                              GLenum        type,
                              GLuint        id,
                              GLenum        severity,
                              GLsizei       length,
                              const GLchar* message,
                              const void*   userParam)
    {
        if (severity != GL_DEBUG_TYPE_ERROR) return;
        LOG_ERROR("({}): {}", type, message);
        DebugBreak();
    }, NULL);

    static const GLfloat points1[] = {
        // Position
        -1.0f, -0.5f, 0.0f, 0.8f, 0.2f, 0.2f,
        0.0f, -0.5f, 0.0f, 0.8f, 0.2f, 0.2f,
        -0.5f, 0.5f, 0.0f, 0.8f, 0.2f, 0.2f,
        0.0f, -0.5f, 0.0f, 0.2f, 0.2f, 0.8f,
        1.0f, -0.5f, 0.0f, 0.2f, 0.2f, 0.8f,
        0.5f, 0.5f, 0.0f, 0.2f, 0.2f, 0.8f,
    };

    GLuint vao, vbo1;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(points1), points1, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 24, NULL);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 24, (void*) 12);
    glEnableVertexAttribArray(1);

    const char* vertexSrc = R"(
    #version 420 core

    layout(location = 0) in vec3 a_position;
    layout(location = 1) in vec3 a_color;

    out vec4 v_color;
    
    void main()
    {
        v_color = vec4(a_color, 1.0);
        gl_Position = vec4(a_position, 1.0);
    }
    
    )";

    const char* fragSrc = R"(
    #version 420 core

    layout(location = 0) out vec4 a_fragColor;

    in vec4 v_color;
    
    void main()
    {
        a_fragColor = v_color;
    }
    )";

    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertexSrc, NULL);
    glCompileShader(vs);

    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragSrc, NULL);
    glCompileShader(fs);

    GLuint shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vs);
    glAttachShader(shaderProgram, fs);

    glLinkProgram(shaderProgram);

    while (!glfwWindowShouldClose(g_window))
    {
        glfwPollEvents();

        glClearColor(0, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(shaderProgram);
        glBindVertexArray(vao);

        glDrawArrays(GL_TRIANGLES, 0, 6);

        glBindVertexArray(GL_NONE);
        glUseProgram(GL_NONE);

        glfwSwapBuffers(g_window);
    }

    glfwTerminate();

    return 0;
}
