#pragma once

#include <GLM/glm.hpp>
#include <GLFW/glfw3.h>

class Game
{
public:
    Game();
    ~Game();

    void run();

protected:
    void initialize();
    void shutdown();

    void loadContent();
    void unloadContent();

    void initImGui();
    void shutdownImGui();

    void ImGuiNewFrame();
    void ImGuiEndFrame();

    void update(float deltaTime);
    void draw(float deltaTime);
    void drawGui(float deltaTime);
private:
    // Stores the main window that the game is running in
    GLFWwindow* m_window{ 0 };
    // Stores the clear color of the game's window
    glm::vec4 m_clearColor{ 0 };
    // Stores the title of the game's window
    char m_windowTitle[32]{ 0 };
};