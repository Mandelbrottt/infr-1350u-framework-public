#include "Game.h"

#include <iostream>

#include <GLFW/glfw3.h>
#include <glad/glad.h>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_glfw.h"

#include "../vendor/ImGuiLoadingItems.h"
#include "../vendor/ImGuiBezier.h"
#include "../vendor/ImGuiHotkey.h"

#include "Logging.h"

Game::Game()
    : m_window(nullptr),
      m_clearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f)),
      m_windowTitle("Game")
{
}

Game::~Game()
{
}

void Game::run()
{
    initialize();
    initImGui();
    loadContent();
    static float prevFrame = (float) glfwGetTime();

    while (!glfwWindowShouldClose(m_window)) 
    {
        glfwPollEvents();

        float thisFrame = (float) glfwGetTime();
        float deltaTime = thisFrame - prevFrame;

        update(deltaTime);
        draw(deltaTime);
        
        ImGuiNewFrame();
        drawGui(deltaTime);
        ImGuiEndFrame();

        // Present our image to windows
        prevFrame = thisFrame;
        glfwSwapBuffers(m_window);
    }
    unloadContent();
    shutdownImGui();
    shutdown();
}

void Game::initialize()
{
    // Initialize GLFW
    if (glfwInit() == GLFW_FALSE)
    {
        std::cout << "Failed to initialize GLFW" << std::endl;
        LOG_ASSERT(false, "GLFW failed to initialize!");
    }

    // Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

    // Create a new GLFW window
    m_window = glfwCreateWindow(600, 600, m_windowTitle, nullptr, nullptr);

    // We want GL commands to be executed for our window, so we make our window's context the current one
    glfwMakeContextCurrent(m_window);

    // Let glad know what function loader we are using (will call gl commands via glfw)
    if (gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) == 0)
    {
        std::cout << "Failed to initialize Glad" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }
}

void Game::shutdown()
{
    glfwTerminate();
}

void Game::loadContent()
{
}

void Game::unloadContent()
{
}

void Game::initImGui()
{
    ImGui::CreateContext();

    ImGuiIO& io = ImGui::GetIO();

    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
    io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

    // Set up the ImGui implementation for OpenGL
    ImGui_ImplGlfw_InitForOpenGL(m_window, true);
    ImGui_ImplOpenGL3_Init("#version 410");

    ImGui::StyleColorsDark();

    ImGuiStyle& style = ImGui::GetStyle();

    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 0.8f;
    }
}

void Game::shutdownImGui()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();

    ImGui::DestroyContext();
}

void Game::ImGuiNewFrame()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();

    ImGui::NewFrame();
}

void Game::ImGuiEndFrame()
{
    // Make sure ImGui knows how big our window is
    ImGuiIO& io = ImGui::GetIO();
    int      width{ 0 }, height{ 0 };
    glfwGetWindowSize(m_window, &width, &height);
    io.DisplaySize = ImVec2(width, height);
    // Render all of our ImGui elements
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    // If we have multiple viewports enabled (can drag into a new window)
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {

        // Update the windows that ImGui is using
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();

        // Restore our gl context
        glfwMakeContextCurrent(m_window);
    }
}

void Game::update(float deltaTime)
{
}

void Game::draw(float deltaTime)
{
    glClearColor(m_clearColor.x, m_clearColor.g, m_clearColor.b, m_clearColor.a);
    glClear(GL_COLOR_BUFFER_BIT);
}

void Game::drawGui(float deltaTime)
{
    ImGui::Begin("Lmao Haha");

    ImGui::ColorEdit4("Clear Color", &m_clearColor[0]);

    if (ImGui::InputText("Window Title", m_windowTitle, 32)) {
        glfwSetWindowTitle(m_window, m_windowTitle);
    }

    ////////////////////////////////////////////////////////////////////

    const ImU32 col = ImGui::GetColorU32(ImGuiCol_ButtonHovered);
    const ImU32 bg = ImGui::GetColorU32(ImGuiCol_Button);

    ImGui::Spinner("##spinner", 15, 6, col);
    ImGui::BufferingBar("##buffer_bar", 0.7f, ImVec2(400, 6), bg, col);

    /////////////////////////////////////////////////////////////////////

    static float v[5] = { 0.390f, 0.575f, 0.565f, 1.000f };
    ImGui::Bezier("easeOutSine", v);
    float y = ImGui::BezierValue(0.5f, v);

    //////////////////////////////////////////////////////////////////

    static std::vector<ImHotKey::HotKey> hotkeys = { { "Layout", "Reorder nodes in a simpler layout", 0xFFFF261D}
        ,{"Save", "Save the current graph", 0xFFFF1F1D}
        ,{"Load", "Load an existing graph file", 0xFFFF181D}
        ,{"Play/Stop", "Play or stop the animation from the current graph", 0xFFFFFF3F}
        ,{"SetKey", "Make a new animation key with the current parameters values at the current time", 0xFFFFFF1F}
    };

    // The editor is a modal window. bring it with something like that
    if (ImGui::Button("Edit Hotkeys"))
    {
        ImGui::OpenPopup("HotKeys Editor");
    }
    ImHotKey::Edit(hotkeys.data(), hotkeys.size(), "HotKeys Editor");

    // ImHotKey also provides a way to retrieve HotKey
    int hotkey = ImHotKey::GetHotKey(hotkeys.data(), hotkeys.size());
    if (hotkey != -1)
    {
        // handle the hotkey index!
    }
    
    ImGui::End();
}
