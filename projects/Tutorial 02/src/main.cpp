#include <iostream>

#include "Game.h"
#include "Logging.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <GLM/glm.hpp>


int main()
{
    Logger::Init();
    
    auto game = new Game();
    game->run();
    delete game;
	
	return 0;
}
