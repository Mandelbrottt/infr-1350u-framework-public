#include "Game.h"

#include <stdexcept>
#include <vector>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.cpp"
#include "imgui_impl_glfw.cpp"

#include "cereal/cereal.hpp"

#include <fmod.hpp>
#include <fmod_common.h>
#include <fmod_errors.h>

#include <GLM/gtc/matrix_transform.hpp>

bool isOrtho = false;

Game::Game()
    : myWindow(nullptr),
      myWindowTitle("Game"),
      myClearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f))
{
}

Game::~Game()
{
}

void Game::Run()
{
    Initialize();
    InitImGui();

    LoadContent();

    FMOD::System* sys;
    FMOD::System_Create(&sys);

    sys->close();
    sys->release();

    static float prevFrame = (float) glfwGetTime();

    // Run as long as the window is open
    while (!glfwWindowShouldClose(myWindow))
    {
        // Poll for events from windows (clicks, key presses, closing, all that)
        glfwPollEvents();

        float thisFrame = (float) glfwGetTime();
        float deltaTime = thisFrame - prevFrame;

        Update(deltaTime);
        Draw(deltaTime);

        ImGuiNewFrame();
        DrawGui(deltaTime);
        ImGuiEndFrame();

        // Present our image to windows
        glfwSwapBuffers(myWindow);
    }

    UnloadContent();

    ShutdownImGui();
    Shutdown();
}

void Game::Initialize()
{
    // Initialize GLFW
    if (glfwInit() == GLFW_FALSE)
    {
        std::cout << "Failed to initialize GLFW" << std::endl;
        throw std::runtime_error("Failed to initialize GLFW");
    }
	
    // Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

    // Create a new GLFW window
    myWindow = glfwCreateWindow(1280, 720, myWindowTitle, nullptr, nullptr);

    // We want GL commands to be executed for our window, so we make our window's context the current one
    glfwMakeContextCurrent(myWindow);

    // Tie our game to our window, so we can access it via callbacks
    glfwSetWindowUserPointer(myWindow, this);
    // Set our window resized callback
    glfwSetWindowSizeCallback(myWindow,
                              [](GLFWwindow* window, int width, int height)
                              {
                                  glViewport(0, 0, width, height);
                              });

    glfwSetKeyCallback(myWindow,
                       [](GLFWwindow* window, int key, int scancode, int action, int mod)
                       {
                           if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
                               isOrtho = !isOrtho;

                           if (key == GLFW_KEY_ESCAPE)
                               glfwSetWindowShouldClose(window, GLFW_TRUE);
                       });
    
    // Let glad know what function loader we are using (will call gl commands via glfw)
    if (gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) == 0)
    {
        std::cout << "Failed to initialize Glad" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }

    glEnable(GL_DEPTH_TEST);
    
}

void Game::Shutdown()
{
    glfwTerminate();
}

void Game::LoadContent()
{    
    // Create a new mesh from the data
    myMesh = std::make_shared<Mesh>("monkey.obj");

    myShader = std::make_shared<Shader>();
    myShader->Load("passthrough.vert", "passthrough.frag");
}

void Game::UnloadContent()
{
}

void Game::InitImGui()
{
    // Creates a new ImGUI context
    ImGui::CreateContext();
    // Gets our ImGUI input/output 
    ImGuiIO& io = ImGui::GetIO();
    // Enable keyboard navigation
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    // Allow docking to our window
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    // Allow multiple viewports (so we can drag ImGui off our window)
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
    // Allow our viewports to use transparent backbuffers
    io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

    // Set up the ImGui implementation for OpenGL
    ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
    ImGui_ImplOpenGL3_Init("#version 410");

    // Dark mode FTW
    ImGui::StyleColorsDark();

    // Get our imgui style
    ImGuiStyle& style = ImGui::GetStyle();
    //style.Alpha = 1.0f;
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 0.8f;
    }
}

void Game::ShutdownImGui()
{
    // Cleanup the ImGui implementation
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    // Destroy our ImGui context
    ImGui::DestroyContext();
}

void Game::ImGuiNewFrame()
{
    // Implementation new frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    // ImGui context new frame
    ImGui::NewFrame();
}

void Game::ImGuiEndFrame()
{
    // Make sure ImGui knows how big our window is
    ImGuiIO& io = ImGui::GetIO();
    int      width{ 0 }, height{ 0 };
    glfwGetWindowSize(myWindow, &width, &height);
    io.DisplaySize = ImVec2((float) width, (float) height);

    // Render all of our ImGui elements
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    // If we have multiple viewports enabled (can drag into a new window)
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        // Update the windows that ImGui is using
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        // Restore our gl context
        glfwMakeContextCurrent(myWindow);
    }
}

void Game::Update(float deltaTime)
{
}

float g_scale = 1.0f;

void Game::Draw(float deltaTime)
{
    int width, height;
    glfwGetWindowSize(myWindow, &width, &height);
    
    // Clear our screen every frame
    glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float fov = (float) width / (float) height;

    glm::mat4 projection;
    if (isOrtho)
        projection = glm::ortho(-1.0f / g_scale * fov, 1.0f / g_scale * fov,
                                -1.0f / g_scale, 1.0f / g_scale,
                                0.0f, 1000.0f);
    else
        projection = glm::perspective(glm::radians(60.0f), fov, 0.1f, 1000.0f);

    glm::mat4 view = glm::lookAt(glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));

    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.0f, 0.0f, -5.0f));
    model           = glm::rotate(model, (float) glfwGetTime(), glm::vec3(-0.0f, 1.0f, 0.0f));
    model           = glm::scale(model, glm::vec3(isOrtho ? 1.0f : g_scale));

    myShader->Bind();
    myShader->SetUniformMat4("u_model", model);
    myShader->SetUniformMat4("u_view", view);
    myShader->SetUniformMat4("u_projection", projection);
    myMesh->Draw();

    model = glm::translate(glm::mat4(1.0f), glm::vec3(2.0f, 0.0f, -5.0f));
    model = glm::rotate(model, (float) glfwGetTime(), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(isOrtho ? 1.0f : g_scale));
    
    myShader->SetUniformMat4("u_model", model);
    myShader->SetUniformMat4("u_view", view);
    myShader->SetUniformMat4("u_projection", projection);
    myMesh->Draw();
}

void Game::DrawGui(float deltaTime)
{
    // Open a new ImGui window
    ImGui::Begin("Test");

    // Draw a color editor
    ImGui::ColorEdit4("Clear Color", &myClearColor[0]);

    ImGui::SliderFloat("Scale", &g_scale, 0.2f, 5.0f);

    ImGui::Checkbox("Orthographic", &isOrtho);

    ImGui::End();
}
