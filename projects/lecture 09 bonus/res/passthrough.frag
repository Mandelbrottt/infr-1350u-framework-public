#version 420

layout(location = 0) in vec3 in_normal;
layout(location = 1) in vec3 in_fragPos;

layout(location = 0) out vec4 out_color;

void main()
{
    if (dot(normalize(in_normal), normalize(-in_fragPos)) < 0.5)
    {
        out_color = vec4(0.0, 0.0, 0.0, 1.0);
    }
    else
    {
        out_color = vec4(in_normal, 1.0);
    }
}