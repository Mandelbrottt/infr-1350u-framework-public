#version 420

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_textureCoord;
layout(location = 2) in vec3 in_normal;

layout(location = 0) out vec3 out_normal;
layout(location = 1) out vec3 out_fragPos;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

void main()
{
    gl_Position = u_projection * u_view * u_model * vec4(in_position, 1.0);
	out_normal = normalize(mat3(inverse(transpose(u_view * u_model))) * in_normal);

    out_fragPos = vec3(u_view * u_model * vec4(in_position, 1.0));
}