#pragma once

#include <glad/glad.h>
#include <GLM/glm.hpp>

#include <cstdint>
#include <memory>
#include <vector>
#include <string>

struct Vertex
{
	glm::vec3 Position;
	glm::vec2 TextureUV;
	glm::vec3 Normal;
};

class Mesh
{
public:
	using Sptr = std::shared_ptr<Mesh>;

    explicit Mesh(const std::string& fileName);
    ~Mesh();

    bool loadFromFile(const std::string& fileName);

    void Draw();

private:
    GLuint myVao = 0;
    GLuint myVbo = 0;
    
    size_t myVertexCount = 0;
    size_t myFaceCount = 0;
};