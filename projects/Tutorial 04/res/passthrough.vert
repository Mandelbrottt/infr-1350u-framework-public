#version 420

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec4 inColor;

layout(location = 0) out vec4 outColor;

uniform mat4 u_mvp;

void main()
{
    gl_Position = u_mvp * vec4(inPosition, 1.0);
	outColor = inColor;
}