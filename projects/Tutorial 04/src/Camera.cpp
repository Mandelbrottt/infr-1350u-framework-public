#include "Camera.h"

#include <GLM/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/quaternion.hpp>

Camera::Camera()
    : Projection(glm::mat4(1.0f)),
      myPosition(glm::vec3(0.0f)),
      myView(glm::mat4(1.0f))
{
}

void Camera::SetPosition(const glm::vec3& pos)
{
    glm::vec3 temp = glm::vec4(-glm::mat3(myView) * pos, 1.0f);
    TransX = temp.x;
    TransY = temp.y;
    TransZ = temp.z;
    myPosition = pos;
}

void Camera::LookAt(const glm::vec3& target, const glm::vec3& up)
{
    myView = glm::lookAt(myPosition, target, up);
}

void Camera::Rotate(const glm::quat& rot)
{
    if (rot != glm::quat(glm::vec3(0.0f)))
    {
        myView = glm::mat4_cast(rot) * myView;
    }
}

void Camera::Move(const glm::vec3& local)
{
    if (local != glm::vec3(0.0f))
    {
        TransX -= local.x;
        TransY -= local.y;
        TransZ -= local.z;

        myPosition = -glm::inverse(glm::mat3(myView)) * glm::vec3(TransX, TransY, TransZ);
    }
}
