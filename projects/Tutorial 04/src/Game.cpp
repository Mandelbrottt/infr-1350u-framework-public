#include "Game.h"

#include <stdexcept>
#include <vector>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.cpp"
#include "imgui_impl_glfw.cpp"

#include "cereal/cereal.hpp"

#include <fmod.hpp>
#include <fmod_common.h>
#include <fmod_errors.h>

#include "GLM/gtc/matrix_transform.hpp"


static void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height);

Game::Game()
    : myWindow(nullptr),
      myWindowTitle("Game"),
      myClearColor(glm::vec4(0.1f, 0.1f, 0.1f, 1.0f))
{
}

Game::~Game()
{
}

void Game::Run()
{
    Initialize();
    InitImGui();

    LoadContent();

    FMOD::System* sys;
    FMOD::System_Create(&sys);

    sys->close();
    sys->release();

    static float prevFrame = (float) glfwGetTime();

    // Run as long as the window is open
    while (!glfwWindowShouldClose(myWindow))
    {
        // Poll for events from windows (clicks, key presses, closing, all that)
        glfwPollEvents();

        float thisFrame = (float) glfwGetTime();
        float deltaTime = thisFrame - prevFrame;

        Update(deltaTime);
        Draw(deltaTime);

        ImGuiNewFrame();
        DrawGui(deltaTime);
        ImGuiEndFrame();

        // Present our image to windows
        glfwSwapBuffers(myWindow);
        prevFrame = thisFrame;
    }

    UnloadContent();

    ShutdownImGui();
    Shutdown();
}

void Game::Initialize()
{
    // Initialize GLFW
    if (glfwInit() == GLFW_FALSE)
    {
        std::cout << "Failed to initialize GLFW" << std::endl;
        throw std::runtime_error("Failed to initialize GLFW");
    }
	
    // Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

    // Create a new GLFW window
    myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);

    // We want GL commands to be executed for our window, so we make our window's context the current one
    glfwMakeContextCurrent(myWindow);

    // Tie our game to our window, so we can access it via callbacks
    glfwSetWindowUserPointer(myWindow, this);
    // Set our window resized callback
    glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);
    
    // Let glad know what function loader we are using (will call gl commands via glfw)
    if (gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) == 0)
    {
        std::cout << "Failed to initialize Glad" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }
}

void Game::Shutdown()
{
    glfwTerminate();
}

void Game::LoadContent()
{
    myCamera = std::make_shared<Camera>();
    myCamera->SetPosition(glm::vec3(5.0f, 5.0f, 5.0f));
    myCamera->LookAt(glm::vec3(0.0f));
    myCamera->Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);

    // Create our 4 vertices
    std::vector<Vertex> vertices = {
        //  Position                Color
        //  x      y      z         r     g     b     a
        { { -0.5f, -0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
        { { 0.5f, -0.5f, 0.0f }, { 1.0f, 1.0f, 0.0f, 1.0f } },
        { { 0.5f, 0.5f, 0.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } },
        { { -0.5f, 0.5f, 0.0f }, { 1.0f, 0.0f, 1.0f, 1.0f } },
    };

    std::vector<GLuint> indices = {
        0, 1, 2,
        0, 2, 3,
    };
    
    // Create a new mesh from the data
    myMesh = std::make_shared<Mesh>(vertices, indices);

    myShader = std::make_shared<Shader>();
    myShader->Load("passthrough.vert", "passthrough.frag");

    myModelTransform = glm::mat4(1.0f);
}

void Game::UnloadContent()
{
}

void Game::InitImGui()
{
    // Creates a new ImGUI context
    ImGui::CreateContext();
    // Gets our ImGUI input/output 
    ImGuiIO& io = ImGui::GetIO();
    // Enable keyboard navigation
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    // Allow docking to our window
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    // Allow multiple viewports (so we can drag ImGui off our window)
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
    // Allow our viewports to use transparent backbuffers
    io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

    // Set up the ImGui implementation for OpenGL
    ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
    ImGui_ImplOpenGL3_Init("#version 410");

    // Dark mode FTW
    ImGui::StyleColorsDark();

    // Get our imgui style
    ImGuiStyle& style = ImGui::GetStyle();
    //style.Alpha = 1.0f;
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 0.8f;
    }
}

void Game::ShutdownImGui()
{
    // Cleanup the ImGui implementation
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    // Destroy our ImGui context
    ImGui::DestroyContext();
}

void Game::ImGuiNewFrame()
{
    // Implementation new frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    // ImGui context new frame
    ImGui::NewFrame();
}

void Game::ImGuiEndFrame()
{
    // Make sure ImGui knows how big our window is
    ImGuiIO& io = ImGui::GetIO();
    int      width{ 0 }, height{ 0 };
    glfwGetWindowSize(myWindow, &width, &height);
    io.DisplaySize = ImVec2((float) width, (float) height);

    // Render all of our ImGui elements
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    // If we have multiple viewports enabled (can drag into a new window)
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        // Update the windows that ImGui is using
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        // Restore our gl context
        glfwMakeContextCurrent(myWindow);
    }
}

void Game::Update(float deltaTime)
{
    static float totalTime = 0.0f;
    totalTime += deltaTime;

    glm::vec3 position = glm::vec3(0.0f);
    glm::vec3 rotation = glm::vec3(0.0f);

    float speed    = 10.0f;
    float rotSpeed = 1.0f;

    if (glfwGetKey(myWindow, GLFW_KEY_W) == GLFW_PRESS)
        position.z -= speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_S) == GLFW_PRESS)
        position.z += speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_A) == GLFW_PRESS)
        position.x -= speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_D) == GLFW_PRESS)
        position.x += speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
        position.y -= speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
        position.y += speed * deltaTime;

    if (glfwGetKey(myWindow, GLFW_KEY_Q) == GLFW_PRESS)
        rotation.z -= rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_E) == GLFW_PRESS)
        rotation.z += rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_UP) == GLFW_PRESS)
        rotation.x -= rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
        rotation.x += rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
        rotation.y -= rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
        rotation.y += rotSpeed * deltaTime;

    myCamera->Rotate(rotation);
    myCamera->Move(position);

    static float d = 0;
    static float v = 0;
    static float a = -1.0f;
    
    if (glfwGetKey(myWindow, GLFW_KEY_M) == GLFW_PRESS && d == 0.0f)
        v = 5.0f;

    v += a * deltaTime;
    d += v * deltaTime;

    if (d <= 0.0f)
    {
        v = 0.0f;
        d = 0.0f;
    }

    a = -10.0f;

    myModelTransform = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, d, 0.0f));
    myModelTransform = glm::rotate(myModelTransform, totalTime, glm::vec3(0.0f, 1.0f, 0.0f));

    if (doOrtho)
        myCamera->Projection = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1000.0f);
    else
        myCamera->Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.0f, 1000.0f);
}

void Game::Draw(float deltaTime)
{
    // Clear our screen every frame
    glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
    glClear(GL_COLOR_BUFFER_BIT);

    myShader->Bind();

    myShader->setUniformMat4("u_mvp", myCamera->GetViewProjection() * myModelTransform);

    myMesh->Draw();
}

void Game::DrawGui(float deltaTime)
{
    // Open a new ImGui window
    ImGui::Begin("Test");

    // Draw a color editor
    ImGui::ColorEdit4("Clear Color", &myClearColor[0]);

    // Check if a textbox has changed, and update our window title if it has
    if (ImGui::InputText("Window Title", myWindowTitle, 32))
    {
        glfwSetWindowTitle(myWindow, myWindowTitle);
    }

    ImGui::End();

    // Open a second ImGui window
    ImGui::Begin("Debug");
    // Draw a formatted text line
    ImGui::Text("Time: %f", glfwGetTime());
    ImGui::End();

    ImGui::Begin("Tutorial 4 Stuffs");

    ImGui::Checkbox("Orthographic", &doOrtho);

    ImGui::End();
}

static void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}
