#include "Shader.h"
#include "Logging.h"

#include <fstream>
#include <sstream>

#include <GLM/gtc/type_ptr.hpp>

Shader::Shader()
{
    myShaderHandle = glCreateProgram();
}

Shader::~Shader()
{
    glDeleteShader(myShaderHandle);
    myShaderHandle = 0;
}

void Shader::Compile(const std::string& vsSource, const std::string& fsSource)
{
    if (vsSource.empty() || fsSource.empty()) return;
    
    GLuint vs = _CompileShaderPart(vsSource.c_str(), GL_VERTEX_SHADER);
    GLuint fs = _CompileShaderPart(fsSource.c_str(), GL_FRAGMENT_SHADER);

    glAttachShader(myShaderHandle, vs);
    glAttachShader(myShaderHandle, fs);

    glLinkProgram(myShaderHandle);

    GLint success = 0;
    glGetProgramiv(myShaderHandle, GL_LINK_STATUS, &success);

    if (success == GL_FALSE)
    {
        GLint length = 0;
        glGetProgramiv(myShaderHandle, GL_INFO_LOG_LENGTH, &length);
        if (length > 0)
        {
            char log[1024];
            glGetProgramInfoLog(myShaderHandle, length, &length, log);

            LOG_ERROR("Shader failed to link:\n{}", log);
        }
        else
        {
            LOG_ERROR("Shader failed to link for an unknown reason!");
        }
        
        // Delete the partial program
        glDeleteProgram(myShaderHandle);
        throw std::runtime_error("Failed to link shader program!");
    }
    else
    {
        LOG_TRACE("Shader has been linked");
    }

    glDetachShader(myShaderHandle, vs);
    glDeleteShader(vs);
    glDetachShader(myShaderHandle, fs);
    glDeleteShader(fs);
}

std::string _readFile(const std::string& fileName)
{
    std::ifstream file(fileName);

    if (file)
    {
        std::stringstream ss;
        ss << file.rdbuf();
        return ss.str();
    }
    else
    {
        LOG_ERROR("File {} failed to load!", fileName);
    }
    return std::string();
}

void Shader::Load(const std::string& vsFile, const std::string& fsFile)
{
    std::string vsSource = _readFile(vsFile);
    std::string fsSource = _readFile(fsFile);

    Compile(vsSource, fsSource);
}

void Shader::Bind()
{
    glUseProgram(myShaderHandle);
}

bool Shader::setUniformMat4(const std::string name, const glm::mat4& v)
{
    GLint loc = glGetUniformLocation(myShaderHandle, name.c_str());
    if (loc != -1)
    {
        glProgramUniformMatrix4fv(myShaderHandle, loc, 1, GL_FALSE, glm::value_ptr(v));
        return true;
    }
    return false;
}

GLuint Shader::_CompileShaderPart(const char* source, GLenum type)
{
    GLuint result = glCreateShader(type);

    glShaderSource(result, 1, &source, NULL);
    glCompileShader(result);

    // Check our compile status
    GLint compileStatus = 0;
    glGetShaderiv(result, GL_COMPILE_STATUS, &compileStatus);

    if (compileStatus == GL_FALSE)
    {
        GLint logSize = 0;
        glGetShaderiv(result, GL_INFO_LOG_LENGTH, &logSize);
        
        // Create a new character buffer for the log
        char* log = new char[logSize];

        glGetShaderInfoLog(result, logSize, &logSize, log);

        LOG_ERROR("Failed to compile shader part:\n{}", log);

        delete[] log;

        glDeleteShader(result);

        throw std::runtime_error("Failed to compile shader part!");
    }
    else
    {
        LOG_TRACE("Shader part has been compiled!");
    }
    // Return the compiled shader part
    return result;
}
