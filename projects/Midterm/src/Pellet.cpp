/*
 * Intro to CG Midterm : Snake
 *
 * Myles Cragg   100704245
 * Kennedy Adams 100632983
 */

#include "Pellet.h"

#include <random>

Pellet::Pellet(glm::ivec2 boardSize)
    : boardSize(boardSize)
{
    reset();
}

void Pellet::reset()
{
    position = glm::ivec2(rand() % boardSize.x,
                          rand() % boardSize.y);
}

void Pellet::draw()
{
    shader->Bind();

    glm::mat4 translation = glm::mat4(1.0f);

    glm::mat4 projection = glm::ortho(0.0f, 50.0f,
                                      50.0f, 0.0f,
                                      1.0f, -1.0f);

    glm::vec2 offset = glm::vec2(1.0f - size) * 0.5f;
    
    translation = glm::translate(glm::mat4(1.0f), glm::vec3(position + offset, 0.0f));
    translation = glm::scale(translation, glm::vec3(size));

    shader->SetUniformMat4("u_model", translation);
    shader->SetUniformMat4("u_projection", projection);

    mesh->Draw();
}

SmallPellet::SmallPellet(glm::ivec2 boardSize)
    : Pellet(boardSize)
{
    SmallPellet::init();
}

void SmallPellet::init()
{
    // Will be different mesh from LargePellet in take-home
    mesh = std::make_shared<Mesh>("banana.obj");

    shader = std::make_shared<Shader>();
    shader->Load("pellet2.vert", "pellet2.frag");

    size = 1.0f;
}

LargePellet::LargePellet(glm::ivec2 boardSize)
    : Pellet(boardSize)
{
    LargePellet::init();
}

void LargePellet::init()
{
    // Wile be different Mesh from SmallPellet in take-home
    mesh = std::make_shared<Mesh>("apple.obj");

    shader = std::make_shared<Shader>();
    shader->Load("pellet.vert", "pellet.frag");

    size = 1.0f;
}
