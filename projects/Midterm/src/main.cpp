/*
 * Intro to CG Midterm : Snake
 *
 * Myles Cragg   100704245
 * Kennedy Adams 100632983
 */

#include "Game.h"
#include "Logging.h"

int main() {
	Logger::Init();

	Game* game = new Game();
	game->Run();
	delete game;

	return 0;
}

