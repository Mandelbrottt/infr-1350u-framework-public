#pragma once

#include <GLM/glm.hpp>
#include "Mesh.h"
#include "Shader.h"

struct Obstacle
{
    explicit Obstacle(glm::ivec2 boardSize);
    virtual ~Obstacle() = default;

    void reset();

    void draw();

    std::vector<glm::vec2> positions;
    glm::ivec2 boardSize;

    float size = 1.0f;

protected:
    void init();

protected:
    Mesh::Sptr mesh;
    Shader::Sptr shader;
};