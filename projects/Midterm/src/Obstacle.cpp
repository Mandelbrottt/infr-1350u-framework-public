#include "Obstacle.h"

/*
 * Intro to CG Midterm : Snake
 *
 * Myles Cragg   100704245
 * Kennedy Adams 100632983
 */

#include <random>

#include <GLM/gtc/matrix_transform.hpp>

Obstacle::Obstacle(glm::ivec2 boardSize)
    : boardSize(boardSize)
{
    init();
    reset();
}

void Obstacle::reset()
{
    positions.clear();
}

void Obstacle::draw()
{
    shader->Bind();

    glm::mat4 translation = glm::mat4(1.0f);

    glm::mat4 projection = glm::ortho(0.0f, 50.0f,
                                      50.0f, 0.0f,
                                      1.0f, -1.0f);

    shader->SetUniformMat4("u_projection", projection);

    for (auto pos : positions)
    {
        translation = glm::translate(glm::mat4(1.0f), glm::vec3(pos, 0.0f));
        translation = glm::scale(translation, glm::vec3(size));

        shader->SetUniformMat4("u_model", translation);

        mesh->Draw();
    }
}

void Obstacle::init()
{
    // Wile be different Mesh from SmallPellet in take-home
    mesh = std::make_shared<Mesh>("square.obj");

    shader = std::make_shared<Shader>();
    shader->Load("obstacle.vert", "obstacle.frag");

    size = 1.0f;
}
