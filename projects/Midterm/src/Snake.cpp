/*
 * Intro to CG Midterm : Snake
 *
 * Myles Cragg   100704245
 * Kennedy Adams 100632983
 */

#include "Snake.h"

Snake::Snake(glm::ivec2 boardSize)
    : boardSize(boardSize), startPosition(boardSize / 2)
{
    init();
    reset();
}

void Snake::init()
{
    mesh = std::make_shared<Mesh>("square.obj");

    shader = std::make_shared<Shader>();
    shader->Load("snake.vert", "snake.frag");
}

void Snake::update(GLFWwindow* window, float dt)
{
    auto checkDir = [&](glm::vec2 moveDir)
    {
        if (body.size() > 1)
        {
            if (body[0] - body[1] == moveDir)
                return false;
        }
        return true;
    };
    
    if ((glfwGetKey(window, GLFW_KEY_W) || glfwGetKey(window, GLFW_KEY_UP)) &&
        checkDir(glm::vec2(0.0f, 1.0f)))
        moveDirs[0] = glm::vec2(0.0f, -1.0f);

    if ((glfwGetKey(window, GLFW_KEY_S) || glfwGetKey(window, GLFW_KEY_DOWN)) &&
        checkDir(glm::vec2(0.0f, -1.0f)))
        moveDirs[0] = glm::vec2(0.0f, 1.0f);

    if ((glfwGetKey(window, GLFW_KEY_A) || glfwGetKey(window, GLFW_KEY_LEFT)) &&
        checkDir(glm::vec2(1.0f, 0.0f)))
        moveDirs[0] = glm::vec2(-1.0f, 0.0f);

    if ((glfwGetKey(window, GLFW_KEY_D) || glfwGetKey(window, GLFW_KEY_RIGHT)) &&
        checkDir(glm::vec2(-1.0f, 0.0f)))
        moveDirs[0] = glm::vec2(1.0f, 0.0f);
}

void Snake::move()
{
    for (unsigned int i = 1; i < moveDirs.size(); i++)
    {
        moveDirs[i] = body[i - 1] - body[i];
    }

    if (addSegment > 0)
    {
        body.push_back(body.back());
        moveDirs.emplace_back();
        addSegment--;
    }

    for (unsigned int i = 0; i < body.size(); i++)
    {
        body[i] += moveDirs[i];

        if (body[i].x < 0)
            body[i].x += boardSize.x;

        if (body[i].y < 0)
            body[i].y += boardSize.y;
        
        if (body[i].x >= boardSize.x)
            body[i].x -= boardSize.x;

        if (body[i].y >= boardSize.y)
            body[i].y -= boardSize.y;
    }
}

void Snake::draw()
{
    shader->Bind();

    glm::mat4 translation = glm::mat4(1.0f);
    for (auto pos : body)
    {
        glm::mat4 projection = glm::ortho(0.0f, (float) boardSize.x,
                                          (float) boardSize.y, 0.0f,
                                          1.0f, -1.0f);

        translation = glm::translate(glm::mat4(1.0f), glm::vec3(pos, 0.0f));

        shader->SetUniformMat4("u_model", translation);
        shader->SetUniformMat4("u_projection", projection);

        mesh->Draw();
    }
}

void Snake::reset()
{
    body.clear();
    moveDirs.clear();
    
    body.emplace_back(startPosition);
    moveDirs.emplace_back(1.0f, 0.0f);
}
