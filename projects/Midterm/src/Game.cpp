/*
 * Intro to CG Midterm : Snake
 * 
 * Myles Cragg   100704245
 * Kennedy Adams 100632983
 */

#include "Game.h"

#include <stdexcept>
#include <vector>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.cpp"
#include "imgui_impl_glfw.cpp"

#include "cereal/cereal.hpp"

#include <fmod.hpp>
#include <fmod_common.h>
#include <fmod_errors.h>

#include <GLM/gtc/matrix_transform.hpp>
#include <ctime>

bool isOrtho = true;

Game::Game()
    : myWindow(nullptr),
      myWindowTitle("Game"),
      myClearColor(glm::vec4(0.1f, 0.1f, 0.1f, 1.0f))
{
}

Game::~Game()
{
}

void Game::Run()
{
    Initialize();
    InitImGui();

    LoadContent();

    FMOD::System* sys;
    FMOD::System_Create(&sys);

    sys->close();
    sys->release();

    glfwSwapInterval(1);

    static float prevFrame = (float) glfwGetTime();

    // Run as long as the window is open
    while (!glfwWindowShouldClose(myWindow))
    {
        // Poll for events from windows (clicks, key presses, closing, all that)
        glfwPollEvents();

        float thisFrame = (float) glfwGetTime();
        float deltaTime = thisFrame - prevFrame;
        prevFrame       = thisFrame;

        Update(deltaTime);
        Draw(deltaTime);

        ImGuiNewFrame();
        DrawGui(deltaTime);
        ImGuiEndFrame();

        // Present our image to windows
        glfwSwapBuffers(myWindow);
    }

    UnloadContent();

    ShutdownImGui();
    Shutdown();
}

void Game::Initialize()
{
    // Initialize GLFW
    if (glfwInit() == GLFW_FALSE)
    {
        std::cout << "Failed to initialize GLFW" << std::endl;
        throw std::runtime_error("Failed to initialize GLFW");
    }
	
    // Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

    // Create a new GLFW window
    myWindow = glfwCreateWindow(800, 800, myWindowTitle, nullptr, nullptr);

    // We want GL commands to be executed for our window, so we make our window's context the current one
    glfwMakeContextCurrent(myWindow);

    // Tie our game to our window, so we can access it via callbacks
    glfwSetWindowUserPointer(myWindow, this);
    // Set our window resized callback
    glfwSetWindowSizeCallback(myWindow,
                              [](GLFWwindow* window, int width, int height)
                              {
                                  glViewport(0, 0, width, height);
                              });

    glfwSetKeyCallback(myWindow,
                       [](GLFWwindow* window, int key, int scancode, int action, int mod)
                       {
                           if (key == GLFW_KEY_ESCAPE)
                               glfwSetWindowShouldClose(window, GLFW_TRUE);
                       });
    
    // Let glad know what function loader we are using (will call gl commands via glfw)
    if (gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) == 0)
    {
        std::cout << "Failed to initialize Glad" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }

    glEnable(GL_DEPTH_TEST);
}

void Game::Shutdown()
{
    glfwTerminate();
}

void Game::LoadContent()
{
    snake = new Snake(myBoardSize);

    smallPellet = new SmallPellet(myBoardSize);
    largePellet = new LargePellet(myBoardSize);

    obstacle = new Obstacle(myBoardSize);
    
    if (rand() % 2)
    {
        useSmallPellet = true;
        smallPellet->reset();
    }
    else
    {
        useSmallPellet = false;
        largePellet->reset();
    }
}

void Game::UnloadContent()
{
}

void Game::InitImGui()
{
    // Creates a new ImGUI context
    ImGui::CreateContext();
    // Gets our ImGUI input/output 
    ImGuiIO& io = ImGui::GetIO();
    // Enable keyboard navigation
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    // Allow docking to our window
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    // Allow multiple viewports (so we can drag ImGui off our window)
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
    // Allow our viewports to use transparent backbuffers
    io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

    // Set up the ImGui implementation for OpenGL
    ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
    ImGui_ImplOpenGL3_Init("#version 410");

    // Dark mode FTW
    ImGui::StyleColorsDark();

    // Get our imgui style
    ImGuiStyle& style = ImGui::GetStyle();
    //style.Alpha = 1.0f;
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 0.8f;
    }
}

void Game::ShutdownImGui()
{
    // Cleanup the ImGui implementation
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    // Destroy our ImGui context
    ImGui::DestroyContext();
}

void Game::ImGuiNewFrame()
{
    // Implementation new frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    // ImGui context new frame
    ImGui::NewFrame();
}

void Game::ImGuiEndFrame()
{
    // Make sure ImGui knows how big our window is
    ImGuiIO& io = ImGui::GetIO();
    int      width{ 0 }, height{ 0 };
    glfwGetWindowSize(myWindow, &width, &height);
    io.DisplaySize = ImVec2((float) width, (float) height);

    // Render all of our ImGui elements
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    // If we have multiple viewports enabled (can drag into a new window)
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        // Update the windows that ImGui is using
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        // Restore our gl context
        glfwMakeContextCurrent(myWindow);
    }
}

void Game::Update(float deltaTime)
{
    const float frameTime        = 1.0f / (float) (snake->body.size() + 10);
    static float       currentFrameTime = 0.0f;

    currentFrameTime += deltaTime;

    snake->update(myWindow, deltaTime);
    while (currentFrameTime >= frameTime)
    {
        currentFrameTime -= frameTime;
        snake->move();

        if (snake->body[0] == (useSmallPellet ? smallPellet->position : largePellet->position))
        {
            int toAdd = useSmallPellet ? 1 : 2;

            snake->addSegment = toAdd;
            myScore += toAdd;

            if (myScore % 2)
            {
                obstacle->positions.emplace_back(rand() % myBoardSize.x, rand() & myBoardSize.y);
            }
            
            if (rand() % 2)
            {
                useSmallPellet = true;

                bool isValid = false;

                while (!isValid)
                {
                    smallPellet->reset();

                    isValid = true;

                    for (auto obs :  obstacle->positions) 
                        if (smallPellet->position == obs)
                        {
                            isValid = false;
                            break;
                        }
                }
            }
            else
            {
                useSmallPellet = false;

                bool isValid = false;

                while (!isValid)
                {
                    largePellet->reset();

                    isValid = true;

                    for (const auto& obs : obstacle->positions)
                        if (largePellet->position == obs)
                        {
                            isValid = false;
                            break;
                        }
                }
            }
        }

        for (auto segmentPos : snake->body)
        {
            if (segmentPos != snake->body.front() &&
                snake->body.front() == segmentPos)
            {
                reset();
                break;
            }
        }

        for (auto obs : obstacle->positions)
        {
            if (snake->body.front() == obs)
            {
                reset();
                break;
            }
        }
    }
}

void Game::Draw(float deltaTime)
{
    int width, height;
    glfwGetWindowSize(myWindow, &width, &height);
    
    // Clear our screen every frame
    glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    snake->draw();
    
    if (useSmallPellet)
        smallPellet->draw();
    else
        largePellet->draw();

    obstacle->draw();
}

void Game::DrawGui(float deltaTime)
{
    // Open a new ImGui window
    auto windowFlags = ImGuiWindowFlags_NoDecoration |
                       ImGuiWindowFlags_AlwaysAutoResize |
                       ImGuiWindowFlags_NoMove |
                       ImGuiWindowFlags_NoBackground;

    ImVec2 windowPos = ImGui::GetWindowPos();

    ImGui::SetNextWindowPos(windowPos);

    ImGui::Begin("##ScoreBoard", nullptr, windowFlags);

    ImGui::Text("Score: %d", myScore);

    ImGui::End();
}

void Game::reset()
{
    snake->reset();
    smallPellet->reset();
    largePellet->reset();
    obstacle->reset();
    
    myScore = 0;
}
