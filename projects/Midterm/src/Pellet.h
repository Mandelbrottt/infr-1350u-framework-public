/*
 * Intro to CG Midterm : Snake
 *
 * Myles Cragg   100704245
 * Kennedy Adams 100632983
 */

#pragma once

#include "Mesh.h"
#include "Shader.h"

#include <vector>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

struct Pellet
{
    explicit Pellet(glm::ivec2 boardSize);
    virtual ~Pellet() = default;
    
    void reset();

    void draw();

    glm::vec2 position;
    glm::ivec2 boardSize;

    float size = 1.0f;

protected:
    virtual void init() = 0;

protected:
    Mesh::Sptr mesh;
    Shader::Sptr shader;
};

struct SmallPellet : public Pellet
{
    explicit SmallPellet(glm::ivec2 boardSize);
    virtual ~SmallPellet() = default;

protected:
    void init() override;
};

struct LargePellet : public Pellet
{
    explicit LargePellet(glm::ivec2 boardSize);
    virtual ~LargePellet() = default;

protected:
    void init() override;
};