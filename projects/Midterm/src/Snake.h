/*
 * Intro to CG Midterm : Snake
 *
 * Myles Cragg   100704245
 * Kennedy Adams 100632983
 */

#pragma once

#include "Mesh.h"
#include "Shader.h"

#include <vector>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

struct Snake
{
    Snake(glm::ivec2 boardSize);
    ~Snake() = default;

    void update(GLFWwindow* window, float dt);
    void move();
    void draw();

    void reset();

    std::vector<glm::vec2> body;
    std::vector<glm::vec2> moveDirs;

    int addSegment = 0;

private:
    void init();

private:
    Mesh::Sptr mesh;
    Shader::Sptr shader;

    glm::ivec2 boardSize;
    glm::vec2 startPosition;
};
