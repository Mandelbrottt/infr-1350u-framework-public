#version 410

layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec2 in_uv;

layout(location = 0) out vec3 out_color;
layout(location = 1) out vec2 out_texUV;

uniform mat4 MVP;

void main() 
{
	out_color = in_color;

	vec3 vertex = in_pos;
	vertex.z = 0.0;
	
	gl_Position = MVP * vec4(vertex, 1.0);
	out_texUV = in_uv;
}
	