#version 410

layout(location = 0) in vec3 in_color;
layout(location = 1) in vec2 in_texUV;

layout(location = 0) out vec4 frag_color;

uniform sampler2D myTextureSampler;

void main() 
{
	//frag_color = vec4(color, 1.0);
	
	vec4 tex = texture(myTextureSampler, in_texUV);
	if (tex.rgb == vec3(0.0, 0.0, 0.0))
		discard;

	frag_color = texture(myTextureSampler, in_texUV);// * vec4(color, 1.0);
}