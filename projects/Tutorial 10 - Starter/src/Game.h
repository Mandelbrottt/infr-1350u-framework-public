#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <functional>

#include "GLM/glm.hpp"

#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"
#include <GLM/detail/_vectorize.hpp>
#include <GLM/detail/_vectorize.hpp>

class Game {
public:
	Game();
	~Game();

	void Run();

	void Resize(int newWidth, int newHeight);

protected:
	void Initialize();
	void Shutdown();

	void LoadContent();
	void UnloadContent();

	void InitImGui();
	void ShutdownImGui();

	void ImGuiNewFrame();
	void ImGuiEndFrame();

	void Update(float deltaTime);
	void Draw(float deltaTime);
	void DrawGui(float deltaTime);

private:
	GLFWwindow* myWindow;
    glm::ivec2  myWindowSize;
	char        myWindowTitle[32];

	glm::vec4   myClearColor;

	Camera::Sptr myCamera;

	glm::mat4   myModelTransform;

    void _RenderScene(glm::ivec4 viewport, const Camera::Sptr& camera);
};
