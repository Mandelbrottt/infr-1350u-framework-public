#include "Mesh.h"

#include <cstddef>
#include <fstream>
#include <GLM/gtc/matrix_integer.hpp>
#include "Logging.h"

Mesh::Mesh(const std::string& fileName)
{
    loadFromFile(fileName);
}

Mesh::~Mesh()
{
    glDeleteBuffers(1, &myVbo);

    glDeleteVertexArrays(1, &myVao);
}

bool Mesh::loadFromFile(const std::string& fileName)
{
    std::ifstream file(fileName);
    if (file)
    {
        std::vector<glm::vec3> positionData;
        std::vector<glm::vec2> textureData;
        std::vector<glm::vec3> normalData;

        std::vector<glm::imat3> faceData;

        std::string line;
        while (!file.eof())
        {
            std::getline(file, line);

            switch (line[0])
            {
                case '#':
                    break;
                case 'v':
                    if (line[1] == ' ')
                    {
                        glm::vec3 temp;

                        int result = sscanf_s(line.c_str(),
                                              "v %f %f %f",
                                              &temp[0],
                                              &temp[1],
                                              &temp[2]);
                        LOG_ASSERT(result == 3, "Not all values read in!");
                        positionData.emplace_back(temp);
                    }
                    else if (line[1] == 't')
                    {
                        glm::vec2 temp;

                        int result = sscanf_s(line.c_str(),
                                              "vt %f %f",
                                              &temp[0],
                                              &temp[1]);
                        LOG_ASSERT(result == 2, "Not all values read in!");
                        textureData.emplace_back(temp);
                    }
                    else if (line[1] == 'n')
                    {
                        glm::vec3 temp;

                        int result = sscanf_s(line.c_str(),
                                              "vn %f %f %f",
                                              &temp[0],
                                              &temp[1],
                                              &temp[2]);
                        LOG_ASSERT(result == 3, "Not all values read in!");
                        normalData.emplace_back(temp);
                    }
                    break;
                case 'f':
                    glm::imat3 temp;

                    int result = sscanf_s(line.c_str(),
                                          "f %d/%d/%d %d/%d/%d %d/%d/%d",
                                          &temp[0][0], &temp[0][1], &temp[0][2],
                                          &temp[1][0], &temp[1][1], &temp[1][2],
                                          &temp[2][0], &temp[2][1], &temp[2][2]);
                    LOG_ASSERT(result == 9, "Not all values read in!");
                    faceData.emplace_back(temp);
                    break;
            }
        }
        file.close();

        std::vector<Vertex> unpackedData;
        unpackedData.reserve(faceData.size() * 3);
        for (auto& face : faceData)
        {
            for (int i = 0; i < 3; i++)
            {
                Vertex vert = {
                    positionData[face[i][0] - 1],
                    textureData[face[i][1] - 1],
                    normalData[face[i][2] - 1]
                };
                unpackedData.emplace_back(vert);
            }
        }

        myFaceCount   = faceData.size();
        myVertexCount = faceData.size() * 3;

        glCreateVertexArrays(1, &myVao);
        glBindVertexArray(myVao);

        glCreateBuffers(1, &myVbo);

        glBindBuffer(GL_ARRAY_BUFFER, myVbo);
        glBufferData(GL_ARRAY_BUFFER,
                     unpackedData.size() * 8 * sizeof(float),
                     unpackedData.data(),
                     GL_STATIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Position));

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, TextureUV));

        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Normal));

        glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
        glBindVertexArray(GL_NONE);

        return true;
    }
    LOG_ERROR("File {} failed to load!", fileName);
    return false;
}

void Mesh::Draw()
{
    glBindVertexArray(myVao);

    glDrawArrays(GL_TRIANGLES, 0, myVertexCount);

    glBindVertexArray(GL_NONE);
}
