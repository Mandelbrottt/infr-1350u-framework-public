#version 420

layout(location = 0) in vec3 in_normal;

layout(location = 0) out vec4 out_color;

void main()
{
    out_color = vec4(0.2, 0.8, 0.8, 1.0);
}