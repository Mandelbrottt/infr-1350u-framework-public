#version 410

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;

layout(location = 0) out vec4 outColor;

uniform vec3 a_CameraPos;

uniform vec3 a_AmbientColor;
uniform float a_AmbientPower;

uniform vec3 a_LightPos;
uniform vec3 a_LightColor;
uniform float a_LightShininess;
uniform float a_LightAttenuation;

void main() 
{
	vec3 norm = normalize(inNormal);
	
	vec3 toLight = a_LightPos - inWorldPos;
	
	float distToLight = length(toLight);
	toLight = normalize(toLight);
	
	vec3 viewDir = normalize(a_CameraPos - inWorldPos);
	vec3 halfDir = normalize(toLight + viewDir);
	
	float specPower = pow(max(dot(norm, halfDir), 0.0), a_LightShininess);
	vec3 specOut = specPower * a_LightColor;
	
	float diffuseFactor = max(dot(norm, toLight), 0);
	vec3 diffuseOut = diffuseFactor * a_LightColor;
		vec3 ambientOut = a_AmbientColor * a_AmbientPower;	
	float attenuation = 1.0 / (1.0 + a_LightAttenuation * pow(distToLight, 2));
	
	vec3 result = (ambientOut + attenuation * (diffuseOut + specOut)) * inColor.xyz;
	
	outColor = vec4(result, inColor.a) /* * a_ColorMultiplier */ ;
}