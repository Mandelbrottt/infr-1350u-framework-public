#include "Game.h"

#include <stdexcept>
#include <vector>
#include <functional>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.cpp"
#include "imgui_impl_glfw.cpp"

#include "cereal/cereal.hpp"

#include <fmod.hpp>
#include <fmod_common.h>
#include <fmod_errors.h>

#include "GLM/gtc/matrix_transform.hpp"

#include "Scene.h"
#include "Components.h"

Game::Game()
    : myWindow(nullptr),
      myWindowTitle("Game"),
      myClearColor(glm::vec4(0.1f, 0.1f, 0.1f, 1.0f))
{
}

Game::~Game()
{
}

void Game::Run()
{
    Initialize();
    InitImGui();

    LoadContent();

    FMOD::System* sys;
    FMOD::System_Create(&sys);

    sys->close();
    sys->release();

    static float prevFrame = (float) glfwGetTime();

    // Run as long as the window is open
    while (!glfwWindowShouldClose(myWindow))
    {
        // Poll for events from windows (clicks, key presses, closing, all that)
        glfwPollEvents();

        float thisFrame = (float) glfwGetTime();
        float deltaTime = thisFrame - prevFrame;

        Update(deltaTime);
        Draw(deltaTime);

        ImGuiNewFrame();
        DrawGui(deltaTime);
        ImGuiEndFrame();

        // Present our image to windows
        glfwSwapBuffers(myWindow);
        prevFrame = thisFrame;
    }

    UnloadContent();

    ShutdownImGui();
    Shutdown();
}

void Game::Initialize()
{
    // Initialize GLFW
    if (glfwInit() == GLFW_FALSE)
    {
        std::cout << "Failed to initialize GLFW" << std::endl;
        throw std::runtime_error("Failed to initialize GLFW");
    }
	
    // Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

    // Create a new GLFW window
    myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);

    // We want GL commands to be executed for our window, so we make our window's context the current one
    glfwMakeContextCurrent(myWindow);

    // Tie our game to our window, so we can access it via callbacks
    glfwSetWindowUserPointer(myWindow, this);
    // Set our window resized callback
    glfwSetWindowSizeCallback(myWindow,
                              [](GLFWwindow* window, int width, int height)
                              {
                                  auto game = (Game*) glfwGetWindowUserPointer(window);
                                  game->Resize((float) width, (float) height);
                              });
    
    // Let glad know what function loader we are using (will call gl commands via glfw)
    if (gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) == 0)
    {
        std::cout << "Failed to initialize Glad" << std::endl;
        throw std::runtime_error("Failed to initialize GLAD");
    }
}

void Game::Shutdown()
{
    glfwTerminate();
}

void Game::LoadContent()
{
    myCamera = std::make_shared<Camera>();
    myCamera->SetPosition(glm::vec3(5.0f, 5.0f, 5.0f));
    myCamera->LookAt(glm::vec3(0.0f));
    myCamera->Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);

    // Create our 4 vertices
    std::vector<Vertex> vertices = {
        // Position                 Color                       Normal
        //  x       y       z         r     g     b     a         x     y     z
        { { -10.0f, -10.0f, 0.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f, 1.0f } },
        { { -10.0f, 10.0f, 0.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f, 1.0f } },
        { { 10.0f, 10.0f, 0.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f, 1.0f } },
        { { 10.0f, -10.0f, 0.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f, 1.0f } },
    };

    std::vector<GLuint> indices = {
        0, 1, 2,
        0, 2, 3,
    };

    SceneManager::RegisterScene("Test");
    SceneManager::RegisterScene("Test2");
    SceneManager::SetCurrentScene("Test");
    
    // Create a new mesh from the data
    myMesh = std::make_shared<Mesh>(vertices, indices);

    Shader::Sptr phong = std::make_shared<Shader>();
    phong->Load("lighting.vert", "lighting.frag");

    Material::Sptr testMat = std::make_shared<Material>(phong);
    testMat->Set("a_LightPos", { 0, 0, 1 });
    testMat->Set("a_LightColor", { 1.0f, 1.0f, 0 });
    testMat->Set("a_AmbientColor", { 1.0f, 0.2f, 0.2f });
    testMat->Set("a_AmbientPower", 0.25f);
    testMat->Set("a_LightSpecPower", 0.5f);
    testMat->Set("a_LightShininess", 256);
    testMat->Set("a_LightAttenuation", 1.0f);

    auto& ecs = GetRegistry("Test");

    entt::entity  e1 = ecs.create();
    MeshRenderer& m1 = ecs.assign<MeshRenderer>(e1);
    m1.Material      = testMat;
    m1.Mesh          = myMesh;

    auto rotate = [](entt::entity e, float dt)
    {
        CurrentRegistry().get_or_assign<TempTransform>(e).EulerRotation += glm::vec3(0, 0, 90 * dt);
    };

    auto& up    = ecs.get_or_assign<UpdateBehaviour>(e1);
    up.Function = rotate;
}

void Game::UnloadContent()
{
}

void Game::InitImGui()
{
    // Creates a new ImGUI context
    ImGui::CreateContext();
    // Gets our ImGUI input/output 
    ImGuiIO& io = ImGui::GetIO();
    // Enable keyboard navigation
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    // Allow docking to our window
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    // Allow multiple viewports (so we can drag ImGui off our window)
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
    // Allow our viewports to use transparent backbuffers
    io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

    // Set up the ImGui implementation for OpenGL
    ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
    ImGui_ImplOpenGL3_Init("#version 410");

    // Dark mode FTW
    ImGui::StyleColorsDark();

    // Get our imgui style
    ImGuiStyle& style = ImGui::GetStyle();
    //style.Alpha = 1.0f;
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 0.8f;
    }
}

void Game::ShutdownImGui()
{
    // Cleanup the ImGui implementation
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    // Destroy our ImGui context
    ImGui::DestroyContext();
}

void Game::ImGuiNewFrame()
{
    // Implementation new frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    // ImGui context new frame
    ImGui::NewFrame();
}

void Game::ImGuiEndFrame()
{
    // Make sure ImGui knows how big our window is
    ImGuiIO& io = ImGui::GetIO();
    int      width{ 0 }, height{ 0 };
    glfwGetWindowSize(myWindow, &width, &height);
    io.DisplaySize = ImVec2((float) width, (float) height);

    // Render all of our ImGui elements
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    // If we have multiple viewports enabled (can drag into a new window)
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        // Update the windows that ImGui is using
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        // Restore our gl context
        glfwMakeContextCurrent(myWindow);
    }
}

void Game::Update(float deltaTime)
{
    static float totalTime = 0.0f;
    totalTime += deltaTime;

    glm::vec3 position = glm::vec3(0.0f);
    glm::vec3 rotation = glm::vec3(0.0f);

    float speed    = 10.0f;
    float rotSpeed = 1.0f;

    if (glfwGetKey(myWindow, GLFW_KEY_W) == GLFW_PRESS)
        position.z -= speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_S) == GLFW_PRESS)
        position.z += speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_A) == GLFW_PRESS)
        position.x -= speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_D) == GLFW_PRESS)
        position.x += speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
        position.y -= speed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
        position.y += speed * deltaTime;

    if (glfwGetKey(myWindow, GLFW_KEY_Q) == GLFW_PRESS)
        rotation.z -= rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_E) == GLFW_PRESS)
        rotation.z += rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_UP) == GLFW_PRESS)
        rotation.x -= rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
        rotation.x += rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
        rotation.y -= rotSpeed * deltaTime;
    if (glfwGetKey(myWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
        rotation.y += rotSpeed * deltaTime;

    myCamera->Rotate(rotation);
    myCamera->Move(position);

    static float d = 0;
    static float v = 0;
    static float a = -1.0f;

    if (glfwGetKey(myWindow, GLFW_KEY_M) == GLFW_PRESS && d == 0.0f)
        v = 5.0f;

    v += a * deltaTime;
    d += v * deltaTime;

    if (d <= 0.0f)
    {
        v = 0.0f;
        d = 0.0f;
    }

    a = -10.0f;

    auto view = CurrentRegistry().view<UpdateBehaviour>();
    for (const auto& e : view)
    {
        auto& func = CurrentRegistry().get<UpdateBehaviour>(e);
        if (func.Function)
        {
            func.Function(e, deltaTime);
        }
    }
}

void Game::Draw(float deltaTime)
{
    // Clear our screen every frame
    glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
    glClear(GL_COLOR_BUFFER_BIT);

    // We'll grab a reference to the ecs to make things easier
    auto& ecs = CurrentRegistry();

    // We sort our mesh renderers based on material properties
    // This will group all of our meshes based on shader first, then material second
    ecs.sort<MeshRenderer>([](const MeshRenderer& lhs, const MeshRenderer& rhs)
    {
        if (rhs.Material == nullptr || rhs.Mesh == nullptr)
            return false;
        else if (lhs.Material == nullptr || lhs.Mesh == nullptr)
            return true;
        else if (lhs.Material->GetShader() != rhs.Material->GetShader())
            return lhs.Material->GetShader() < rhs.Material->GetShader();
        else
            return lhs.Material < rhs.Material;
    });

    // These will keep track of the current shader and material that we have bound
    Material::Sptr mat         = nullptr;
    Shader::Sptr   boundShader = nullptr;

    // A view will let us iterate over all of our entities that have the given component types
    auto view = ecs.view<MeshRenderer>();

    for (const auto& entity : view)
    {
        // Get our shader
        const MeshRenderer& renderer = ecs.get<MeshRenderer>(entity);

        // Early bail if mesh is invalid
        if (renderer.Mesh == nullptr || renderer.Material == nullptr)
            continue;
        // If our shader has changed, we need to bind it and update our frame-level uniforms
        if (renderer.Material->GetShader() != boundShader)
        {
            boundShader = renderer.Material->GetShader();
            boundShader->Bind();
            boundShader->SetUniform("a_CameraPos", myCamera->GetPosition());
        }
        // If our material has changed, we need to apply it to the shader
        if (renderer.Material != mat)
        {
            mat = renderer.Material;
            mat->Apply();
        }

        // We'll need some info about the entities position in the world
        const TempTransform& transform = ecs.get_or_assign<TempTransform>(entity);
        // Get the object's transformation
        glm::mat4 worldTransform = transform.GetWorldTransform();

        // Our normal matrix is the inverse-transpose of our object's world rotation
        glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(worldTransform)));

        // Update the MVP using the item's transform
        mat->GetShader()->SetUniform("a_ModelViewProjection",
                                     myCamera->GetViewProjection() * worldTransform);

        // Update the model matrix to the item's world transform
        mat->GetShader()->SetUniform("a_Model", worldTransform);

        // Update the model matrix to the item's world transform
        mat->GetShader()->SetUniform("a_NormalMatrix", normalMatrix);
        // Draw the item
        renderer.Mesh->Draw();
    }
}

void Game::DrawGui(float deltaTime)
{
    // Open a new ImGui window
    ImGui::Begin("Graphics Properties");

    // Draw a color editor
    ImGui::ColorEdit4("Clear Color", &myClearColor[0]);

    ImGui::End();

    // Open a second ImGui window
    ImGui::Begin("Debug");
    // Draw a formatted text line
    ImGui::Text("Time: %f", glfwGetTime());
    ImGui::End();

    ImGui::Begin("Scenes");

    auto it = SceneManager::Each();
    for (auto& kvp : it)
    {
        if (ImGui::Button(kvp.first.c_str()))
        {
            SceneManager::SetCurrentScene(kvp.first);
        }
    }

    ImGui::End();
}

void Game::Resize(float width, float height)
{
    glViewport(0, 0, width, height);
    myCamera->Projection = glm::perspective(glm::radians(60.0f), width / height, 0.01f, 1000.0f);
}
