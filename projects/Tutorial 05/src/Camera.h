#pragma once

#include <memory>

#include <GLM/glm.hpp>
#include <GLM/gtc/quaternion.hpp>

class Camera
{
public:
    using Sptr = std::shared_ptr<Camera>;

    Camera();
    virtual ~Camera() = default;

    glm::mat4        Projection;
    const glm::mat4& GetView() const { return myView; }
    glm::mat4        GetViewProjection() const { return Projection * myView; }
    const glm::vec3& GetPosition() const { return myPosition; }
    void             SetPosition(const glm::vec3& pos);
    inline glm::vec3 GetForward() const { return glm::vec3(FrontX, FrontY, FrontZ); }
    inline glm::vec3 GetUp() const { return glm::vec3(UpX, UpY, UpZ); }
    inline glm::vec3 GetRight() const { return glm::vec3(RightX, RightY, RightZ); }
    void             LookAt(const glm::vec3& target, const glm::vec3& up = glm::vec3(0, 1, 0));
    void             Rotate(const glm::quat& rot);
    void             Rotate(const glm::vec3& rot) { Rotate(glm::quat(rot)); }
    void             Move(const glm::vec3& local);
protected:
    glm::vec3 myPosition;

    union
    {
        glm::mat4 myView;

        struct
        {
            //float RightX, RightY, RightZ, TransX;
            //float UpX,    UpY,    UpZ,    TransY;
            //float FrontX, FrontY, FrontZ, TransZ;
            //float M40,    M41,    M42,    M43;

            float RightX, RightY, RightZ, M03;
            float UpX,    UpY,    UpZ,    M13;
            float FrontX, FrontY, FrontZ, M23;
            float TransX, TransY, TransZ, M33;
        };
    };
};
