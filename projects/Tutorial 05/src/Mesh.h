#pragma once

#include <glad/glad.h>
#include <GLM/glm.hpp>
#include <cstdint>
#include <memory>
#include <vector>

struct Vertex
{
	glm::vec3 Position;
	glm::vec4 Color;
    glm::vec3 Normal;
};

class Mesh
{
public:
	using Sptr = std::shared_ptr<Mesh>;

    explicit Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices);
    explicit Mesh(const Vertex* vertices, size_t nVertices, const float* indices, size_t nIndices);
    ~Mesh();

    void Draw();

private:
    GLuint myVao = 0;

    GLuint myVbo = 0;
    GLuint myEbo = 0;
    
    size_t myVertexCount = 0;
    size_t myIndexCount  = 0;
};