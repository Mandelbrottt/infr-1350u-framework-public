#pragma once

#include <memory>
#include <string>

#include <glad/glad.h>

#include <GLM/glm.hpp>

class Shader
{
public:
    using Sptr = std::shared_ptr<Shader>;

    explicit Shader();
    ~Shader();

    // Compile the glsl source code into bytecode useable by OpenGL
    void Compile(const std::string& vsSource, const std::string& fsSource);

    // Load from a file the glsl source code
    void Load(const std::string& vsFile, const std::string& fsFile);

    // Bind the shader in OpenGL
    void Bind();

    void SetUniform(const char* name, const glm::mat4& value);
    void SetUniform(const char* name, const glm::mat3& value);
    void SetUniform(const char* name, const glm::vec4& value);
    void SetUniform(const char* name, const glm::vec3& value);
    void SetUniform(const char* name, float value);
    
private:
    // Compile a specific part of a shader into bytecode
    GLuint _CompileShaderPart(const char* source, GLenum type);

    GLuint myShaderHandle = GL_NONE;
};
