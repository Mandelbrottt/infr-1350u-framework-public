#version 410

layout(location = 0) in vec3 vertex_pos;
layout(location = 1) in vec3 vertex_color;
layout(location = 2) in vec2 vextex_uv;

out vec3 color;
out vec2 texUV;

uniform mat4 MVP;

uniform sampler2D myHeightMap;

uniform float time;

void main() 
{
	color = vertex_color;
	
	vec3 v = vertex_pos;

	// v.y = texture(myHeightMap, vextex_uv).r;

	v.y = cos(2.0 * v.x * v.z + time * 0.5) * sin(5.0 * v.x * v.z + time) * 0.1;

	gl_Position = MVP * vec4(v, 1.0);
	texUV = vextex_uv;
}
	