#version 410

layout(location = 0) in vec3 inWorldPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;

in vec3 camDirection;
in vec3 lightDirection;

uniform vec3 lightWorldPos;

layout(location = 0) out vec4 frag_color;

void main() 
{
	vec3 lightColor = vec3(1.0, 1.0, 1.0);
	float lightIntensity = 5.0f;

	vec3 diffuseColor = inColor;
	vec3 ambientColor = vec3(0.0, 0.0, 0.0);
	vec3 specularColor = vec3(0.5, 0.5, 0.5);

	// Normal
	vec3 n = normalize(inNormal);
	vec3 l = normalize(lightDirection);
	vec3 e = normalize(camDirection);

	// Halfway vector
	vec3 h = normalize(l + e);

	// Specular intensity
	float spec = pow(max(dot(n, h), 0.0), 5);

	// Specular component
	vec3 specOut = spec * specularColor;

	float dist = length(lightWorldPos - inWorldPosition);

	// Diffuse Component
	float diffuseComponent = max(dot(n, l), 0.0);
	vec3 diffuseOut = (diffuseComponent * diffuseColor) / (dist * dist);

	vec3 ambientOut = ambientColor * 0.0;

	vec3 result = ambientOut + diffuseOut + specOut;
	result *= lightIntensity * inColor.xyz;

	frag_color;
}