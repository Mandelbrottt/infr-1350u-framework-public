#version 410

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;

layout(location = 0) out vec3 outWorldPos;
layout(location = 1) out vec3 outColor;
layout(location = 2) out vec3 outNormal;

out vec3 camDirection;
out vec3 lightDirection;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 lightWorldPos;

void main() {
	gl_Position = projection * model * view * vec4(inPosition, 1.0);
 
    // Pass vertex pos to frag shader
    outWorldPos = (model * vec4(inPosition, 1.0)).xyz;

    // We need camera direction
    vec3 vertexPos = (view * model * vec4(inPosition, 1.0)).xyz;

    camDirection = -vertexPos;

    vec3 lightPos = (view * model * vec4(lightWorldPos, 1)).xyz;
    lightDirection = lightPos + camDirection;

    outNormal = (view * model * vec4(inNormal, 1.0)).xyz;
}
	